﻿namespace AnamSys
{
    partial class principalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.planoDeAtendimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avaliaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.anamneseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.anamneseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.faturarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fichaEvolutivaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cad1DeletaBt = new System.Windows.Forms.Button();
            this.cad1NovoBt = new System.Windows.Forms.Button();
            this.cad1AtualizaBt = new System.Windows.Forms.Button();
            this.cad1UfTb = new System.Windows.Forms.TextBox();
            this.cad1CidadeTb = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cad1ObsTb = new System.Windows.Forms.TextBox();
            this.cad1BairroTb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cad1EnderecoTb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cad1NomeTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cad1RgMtb = new System.Windows.Forms.MaskedTextBox();
            this.cad1CpfMtb = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cad1IdMtb = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.conAvalTb = new System.Windows.Forms.TextBox();
            this.conDetailsPn = new System.Windows.Forms.Panel();
            this.conDetLpLb = new System.Windows.Forms.Label();
            this.conDetParCh = new System.Windows.Forms.CheckBox();
            this.conDetXLb = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.conDetNomeLb = new System.Windows.Forms.Label();
            this.conDetIdLb = new System.Windows.Forms.Label();
            this.conDetParPn = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.conDetParNup = new System.Windows.Forms.NumericUpDown();
            this.conDetParLbox = new System.Windows.Forms.ListBox();
            this.conDetDetTb = new System.Windows.Forms.TextBox();
            this.conDetParTotTb = new System.Windows.Forms.TextBox();
            this.conDetParTb = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.conDataDtp = new System.Windows.Forms.DateTimePicker();
            this.conHoraCb = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.conMinCb = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.conDetAddLb = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.conDetFormaCb = new System.Windows.Forms.ComboBox();
            this.ConDetPgCh = new System.Windows.Forms.CheckBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.conDetValorTb = new System.Windows.Forms.TextBox();
            this.conNovoBt = new System.Windows.Forms.Button();
            this.conNovaBt = new System.Windows.Forms.Button();
            this.conPacienteLb = new System.Windows.Forms.Label();
            this.conTodasLb = new System.Windows.Forms.Label();
            this.agentaPn = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.btnProximo = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtAno = new System.Windows.Forms.MaskedTextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cboMes = new System.Windows.Forms.ComboBox();
            this.labelMes = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.conLv = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label16 = new System.Windows.Forms.Label();
            this.conPlanoAtualizaBt = new System.Windows.Forms.Button();
            this.conPlanoTb = new System.Windows.Forms.TextBox();
            this.conParCb = new System.Windows.Forms.ComboBox();
            this.conFinParDtp = new System.Windows.Forms.DateTimePicker();
            this.conFinChequeRb = new System.Windows.Forms.RadioButton();
            this.conFinCartRb = new System.Windows.Forms.RadioButton();
            this.conFinDinRb = new System.Windows.Forms.RadioButton();
            this.conFinValorTb = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.conFinLv = new System.Windows.Forms.ListView();
            this.conFinNomeCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.conFinDataCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.conFinParCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.conFinFormaCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.conFinPendenciaCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel5 = new System.Windows.Forms.Panel();
            this.conFinVezesNup = new System.Windows.Forms.NumericUpDown();
            this.conFinParRb = new System.Windows.Forms.RadioButton();
            this.conFinCredVistaRb = new System.Windows.Forms.RadioButton();
            this.conFinDebRb = new System.Windows.Forms.RadioButton();
            this.conFinAddOpLb = new System.Windows.Forms.Label();
            this.conTaxNameLb = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.conFinConsultaLb = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.conFinPacienteLb = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.conSalvaLb = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.conLimparLb = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.conOperPn = new System.Windows.Forms.Panel();
            this.conTaxNameCb = new System.Windows.Forms.ComboBox();
            this.conTaxEsteCh = new System.Windows.Forms.CheckBox();
            this.conTaxSaveBt = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.conTaxName2Tb = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.conTaxPn = new System.Windows.Forms.Panel();
            this.conTx12Tb = new System.Windows.Forms.TextBox();
            this.conTx9Tb = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.conTx5Tb = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.conTx10Tb = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.conTx11Tb = new System.Windows.Forms.TextBox();
            this.conTx7Tb = new System.Windows.Forms.TextBox();
            this.conTx8Tb = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.conTx3Tb = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.conTx4Tb = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.conTx6Tb = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.conTx2Tb = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.conTx1Tb = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.conTx0Tb = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.cad1Pn = new System.Windows.Forms.Panel();
            this.cad1ConsultasBt = new System.Windows.Forms.Button();
            this.cad1AntLb = new System.Windows.Forms.Label();
            this.cad1LimpaLb = new System.Windows.Forms.Label();
            this.cad1XLb = new System.Windows.Forms.Label();
            this.cad1ProxLv = new System.Windows.Forms.Label();
            this.cadNascDtp = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.consultasPn = new System.Windows.Forms.Panel();
            this.finPn = new System.Windows.Forms.Panel();
            this.finLpLb = new System.Windows.Forms.Label();
            this.finXLb = new System.Windows.Forms.Label();
            this.planoPn = new System.Windows.Forms.Panel();
            this.planoXLb = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.planoNomeLb = new System.Windows.Forms.Label();
            this.avalPn = new System.Windows.Forms.Panel();
            this.avalXLb = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.avalNomeLb = new System.Windows.Forms.Label();
            this.fichaEvolutivaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.conDetailsPn.SuspendLayout();
            this.conDetParPn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.conDetParNup)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel12.SuspendLayout();
            this.agentaPn.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.conFinVezesNup)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.conOperPn.SuspendLayout();
            this.conTaxPn.SuspendLayout();
            this.cad1Pn.SuspendLayout();
            this.consultasPn.SuspendLayout();
            this.finPn.SuspendLayout();
            this.planoPn.SuspendLayout();
            this.avalPn.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrosToolStripMenuItem,
            this.anamneseToolStripMenuItem,
            this.agendaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(198, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastrosToolStripMenuItem
            // 
            this.cadastrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pacienteToolStripMenuItem,
            this.planoDeAtendimentoToolStripMenuItem,
            this.avaliaçãoToolStripMenuItem,
            this.fichaEvolutivaToolStripMenuItem1});
            this.cadastrosToolStripMenuItem.Name = "cadastrosToolStripMenuItem";
            this.cadastrosToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.cadastrosToolStripMenuItem.Text = "Paciente";
            // 
            // pacienteToolStripMenuItem
            // 
            this.pacienteToolStripMenuItem.Name = "pacienteToolStripMenuItem";
            this.pacienteToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.pacienteToolStripMenuItem.Text = "Cadastro";
            this.pacienteToolStripMenuItem.Click += new System.EventHandler(this.pacienteToolStripMenuItem_Click);
            // 
            // planoDeAtendimentoToolStripMenuItem
            // 
            this.planoDeAtendimentoToolStripMenuItem.Name = "planoDeAtendimentoToolStripMenuItem";
            this.planoDeAtendimentoToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.planoDeAtendimentoToolStripMenuItem.Text = "Plano de Atendimento";
            this.planoDeAtendimentoToolStripMenuItem.Click += new System.EventHandler(this.planoDeAtendimentoToolStripMenuItem_Click);
            // 
            // avaliaçãoToolStripMenuItem
            // 
            this.avaliaçãoToolStripMenuItem.Name = "avaliaçãoToolStripMenuItem";
            this.avaliaçãoToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.avaliaçãoToolStripMenuItem.Text = "Avaliação";
            this.avaliaçãoToolStripMenuItem.Click += new System.EventHandler(this.avaliaçãoToolStripMenuItem_Click);
            // 
            // anamneseToolStripMenuItem
            // 
            this.anamneseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem,
            this.anamneseToolStripMenuItem1,
            this.faturarToolStripMenuItem,
            this.fichaEvolutivaToolStripMenuItem});
            this.anamneseToolStripMenuItem.Name = "anamneseToolStripMenuItem";
            this.anamneseToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.anamneseToolStripMenuItem.Text = "Consulta";
            this.anamneseToolStripMenuItem.Click += new System.EventHandler(this.anamneseToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.consultarToolStripMenuItem.Text = "Consultas";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // anamneseToolStripMenuItem1
            // 
            this.anamneseToolStripMenuItem1.Name = "anamneseToolStripMenuItem1";
            this.anamneseToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.anamneseToolStripMenuItem1.Text = "Anamnese";
            this.anamneseToolStripMenuItem1.Click += new System.EventHandler(this.anamneseToolStripMenuItem1_Click);
            // 
            // faturarToolStripMenuItem
            // 
            this.faturarToolStripMenuItem.Name = "faturarToolStripMenuItem";
            this.faturarToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.faturarToolStripMenuItem.Text = "Faturar";
            this.faturarToolStripMenuItem.Click += new System.EventHandler(this.faturarToolStripMenuItem_Click);
            // 
            // fichaEvolutivaToolStripMenuItem
            // 
            this.fichaEvolutivaToolStripMenuItem.Name = "fichaEvolutivaToolStripMenuItem";
            this.fichaEvolutivaToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.fichaEvolutivaToolStripMenuItem.Text = "Ficha Evolutiva";
            this.fichaEvolutivaToolStripMenuItem.Click += new System.EventHandler(this.fichaEvolutivaToolStripMenuItem_Click);
            // 
            // agendaToolStripMenuItem
            // 
            this.agendaToolStripMenuItem.Name = "agendaToolStripMenuItem";
            this.agendaToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.agendaToolStripMenuItem.Text = "Agenda";
            this.agendaToolStripMenuItem.Click += new System.EventHandler(this.agendaToolStripMenuItem_Click);
            // 
            // cad1DeletaBt
            // 
            this.cad1DeletaBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1DeletaBt.Location = new System.Drawing.Point(534, 310);
            this.cad1DeletaBt.Name = "cad1DeletaBt";
            this.cad1DeletaBt.Size = new System.Drawing.Size(75, 25);
            this.cad1DeletaBt.TabIndex = 3;
            this.cad1DeletaBt.Text = "Deleta";
            this.cad1DeletaBt.UseVisualStyleBackColor = true;
            this.cad1DeletaBt.Click += new System.EventHandler(this.cad1DeletaBt_Click);
            // 
            // cad1NovoBt
            // 
            this.cad1NovoBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1NovoBt.Location = new System.Drawing.Point(278, 310);
            this.cad1NovoBt.Name = "cad1NovoBt";
            this.cad1NovoBt.Size = new System.Drawing.Size(75, 25);
            this.cad1NovoBt.TabIndex = 3;
            this.cad1NovoBt.Text = "Novo";
            this.cad1NovoBt.UseVisualStyleBackColor = true;
            this.cad1NovoBt.Click += new System.EventHandler(this.cad1NovoBt_Click);
            // 
            // cad1AtualizaBt
            // 
            this.cad1AtualizaBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1AtualizaBt.Location = new System.Drawing.Point(22, 310);
            this.cad1AtualizaBt.Name = "cad1AtualizaBt";
            this.cad1AtualizaBt.Size = new System.Drawing.Size(75, 25);
            this.cad1AtualizaBt.TabIndex = 3;
            this.cad1AtualizaBt.Text = "Atualiza";
            this.cad1AtualizaBt.UseVisualStyleBackColor = true;
            this.cad1AtualizaBt.Click += new System.EventHandler(this.cad1AtualizaBt_Click);
            // 
            // cad1UfTb
            // 
            this.cad1UfTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1UfTb.Location = new System.Drawing.Point(556, 166);
            this.cad1UfTb.MaxLength = 2;
            this.cad1UfTb.Name = "cad1UfTb";
            this.cad1UfTb.Size = new System.Drawing.Size(53, 26);
            this.cad1UfTb.TabIndex = 2;
            // 
            // cad1CidadeTb
            // 
            this.cad1CidadeTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1CidadeTb.Location = new System.Drawing.Point(321, 166);
            this.cad1CidadeTb.Name = "cad1CidadeTb";
            this.cad1CidadeTb.Size = new System.Drawing.Size(229, 26);
            this.cad1CidadeTb.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(549, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Estado";
            // 
            // cad1ObsTb
            // 
            this.cad1ObsTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1ObsTb.Location = new System.Drawing.Point(22, 207);
            this.cad1ObsTb.Multiline = true;
            this.cad1ObsTb.Name = "cad1ObsTb";
            this.cad1ObsTb.Size = new System.Drawing.Size(587, 86);
            this.cad1ObsTb.TabIndex = 2;
            this.cad1ObsTb.TextChanged += new System.EventHandler(this.cad1ObsTb_TextChanged);
            // 
            // cad1BairroTb
            // 
            this.cad1BairroTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1BairroTb.Location = new System.Drawing.Point(22, 166);
            this.cad1BairroTb.Name = "cad1BairroTb";
            this.cad1BairroTb.Size = new System.Drawing.Size(286, 26);
            this.cad1BairroTb.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(317, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Cidade";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(19, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Observações";
            // 
            // cad1EnderecoTb
            // 
            this.cad1EnderecoTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1EnderecoTb.Location = new System.Drawing.Point(22, 122);
            this.cad1EnderecoTb.Name = "cad1EnderecoTb";
            this.cad1EnderecoTb.Size = new System.Drawing.Size(587, 26);
            this.cad1EnderecoTb.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Bairro";
            // 
            // cad1NomeTb
            // 
            this.cad1NomeTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1NomeTb.Location = new System.Drawing.Point(21, 76);
            this.cad1NomeTb.Name = "cad1NomeTb";
            this.cad1NomeTb.Size = new System.Drawing.Size(483, 26);
            this.cad1NomeTb.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Endereço";
            // 
            // cad1RgMtb
            // 
            this.cad1RgMtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1RgMtb.Location = new System.Drawing.Point(262, 34);
            this.cad1RgMtb.Mask = "0000000000";
            this.cad1RgMtb.Name = "cad1RgMtb";
            this.cad1RgMtb.Size = new System.Drawing.Size(98, 26);
            this.cad1RgMtb.TabIndex = 1;
            // 
            // cad1CpfMtb
            // 
            this.cad1CpfMtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1CpfMtb.Location = new System.Drawing.Point(106, 34);
            this.cad1CpfMtb.Mask = "00000000000";
            this.cad1CpfMtb.Name = "cad1CpfMtb";
            this.cad1CpfMtb.Size = new System.Drawing.Size(107, 26);
            this.cad1CpfMtb.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(259, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "RG";
            // 
            // cad1IdMtb
            // 
            this.cad1IdMtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1IdMtb.Location = new System.Drawing.Point(21, 34);
            this.cad1IdMtb.Mask = "00000";
            this.cad1IdMtb.Name = "cad1IdMtb";
            this.cad1IdMtb.Size = new System.Drawing.Size(35, 26);
            this.cad1IdMtb.TabIndex = 1;
            this.cad1IdMtb.ValidatingType = typeof(int);
            this.cad1IdMtb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cad1IdMtb_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(103, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "CPF";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // conAvalTb
            // 
            this.conAvalTb.Location = new System.Drawing.Point(17, 26);
            this.conAvalTb.Multiline = true;
            this.conAvalTb.Name = "conAvalTb";
            this.conAvalTb.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.conAvalTb.Size = new System.Drawing.Size(600, 363);
            this.conAvalTb.TabIndex = 2;
            // 
            // conDetailsPn
            // 
            this.conDetailsPn.BackColor = System.Drawing.Color.LightBlue;
            this.conDetailsPn.Controls.Add(this.conDetLpLb);
            this.conDetailsPn.Controls.Add(this.conDetParCh);
            this.conDetailsPn.Controls.Add(this.conDetXLb);
            this.conDetailsPn.Controls.Add(this.label54);
            this.conDetailsPn.Controls.Add(this.label52);
            this.conDetailsPn.Controls.Add(this.conDetNomeLb);
            this.conDetailsPn.Controls.Add(this.conDetIdLb);
            this.conDetailsPn.Controls.Add(this.conDetParPn);
            this.conDetailsPn.Controls.Add(this.conDetParNup);
            this.conDetailsPn.Controls.Add(this.conDetParLbox);
            this.conDetailsPn.Controls.Add(this.conDetDetTb);
            this.conDetailsPn.Controls.Add(this.conDetParTotTb);
            this.conDetailsPn.Controls.Add(this.conDetParTb);
            this.conDetailsPn.Controls.Add(this.panel10);
            this.conDetailsPn.Controls.Add(this.label26);
            this.conDetailsPn.Controls.Add(this.conDetAddLb);
            this.conDetailsPn.Controls.Add(this.label55);
            this.conDetailsPn.Controls.Add(this.panel12);
            this.conDetailsPn.Location = new System.Drawing.Point(214, 3);
            this.conDetailsPn.Name = "conDetailsPn";
            this.conDetailsPn.Size = new System.Drawing.Size(467, 427);
            this.conDetailsPn.TabIndex = 75;
            this.conDetailsPn.Visible = false;
            this.conDetailsPn.VisibleChanged += new System.EventHandler(this.conDetailsPn_VisibleChanged);
            this.conDetailsPn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.control_MouseDown);
            this.conDetailsPn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.control_MouseMove);
            this.conDetailsPn.MouseUp += new System.Windows.Forms.MouseEventHandler(this.control_MouseUp);
            // 
            // conDetLpLb
            // 
            this.conDetLpLb.AutoSize = true;
            this.conDetLpLb.Location = new System.Drawing.Point(430, 7);
            this.conDetLpLb.Name = "conDetLpLb";
            this.conDetLpLb.Size = new System.Drawing.Size(19, 13);
            this.conDetLpLb.TabIndex = 0;
            this.conDetLpLb.Text = "Lp";
            this.conDetLpLb.Click += new System.EventHandler(this.conDetLpLb_Click);
            // 
            // conDetParCh
            // 
            this.conDetParCh.AutoSize = true;
            this.conDetParCh.Location = new System.Drawing.Point(564, 155);
            this.conDetParCh.Name = "conDetParCh";
            this.conDetParCh.Size = new System.Drawing.Size(41, 17);
            this.conDetParCh.TabIndex = 81;
            this.conDetParCh.Text = "PG";
            this.conDetParCh.UseVisualStyleBackColor = true;
            this.conDetParCh.Visible = false;
            // 
            // conDetXLb
            // 
            this.conDetXLb.AutoSize = true;
            this.conDetXLb.Location = new System.Drawing.Point(450, 7);
            this.conDetXLb.Name = "conDetXLb";
            this.conDetXLb.Size = new System.Drawing.Size(14, 13);
            this.conDetXLb.TabIndex = 0;
            this.conDetXLb.Text = "X";
            this.conDetXLb.Click += new System.EventHandler(this.conDetXLb_Click);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(3, 115);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(91, 17);
            this.label54.TabIndex = 3;
            this.label54.Text = "Consulta N°: ";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(183, 1);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(90, 25);
            this.label52.TabIndex = 0;
            this.label52.Text = "Consulta";
            // 
            // conDetNomeLb
            // 
            this.conDetNomeLb.AutoSize = true;
            this.conDetNomeLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conDetNomeLb.Location = new System.Drawing.Point(3, 133);
            this.conDetNomeLb.Name = "conDetNomeLb";
            this.conDetNomeLb.Size = new System.Drawing.Size(124, 17);
            this.conDetNomeLb.TabIndex = 3;
            this.conDetNomeLb.Text = "Nome do Paciente";
            // 
            // conDetIdLb
            // 
            this.conDetIdLb.AutoSize = true;
            this.conDetIdLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conDetIdLb.Location = new System.Drawing.Point(89, 111);
            this.conDetIdLb.Name = "conDetIdLb";
            this.conDetIdLb.Size = new System.Drawing.Size(81, 25);
            this.conDetIdLb.TabIndex = 0;
            this.conDetIdLb.Text = "Numero";
            // 
            // conDetParPn
            // 
            this.conDetParPn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.conDetParPn.Controls.Add(this.button2);
            this.conDetParPn.Controls.Add(this.button1);
            this.conDetParPn.Location = new System.Drawing.Point(373, 178);
            this.conDetParPn.Name = "conDetParPn";
            this.conDetParPn.Size = new System.Drawing.Size(88, 73);
            this.conDetParPn.TabIndex = 80;
            this.conDetParPn.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(8, 45);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "Remover";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Pago";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // conDetParNup
            // 
            this.conDetParNup.Location = new System.Drawing.Point(514, 152);
            this.conDetParNup.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.conDetParNup.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.conDetParNup.Name = "conDetParNup";
            this.conDetParNup.Size = new System.Drawing.Size(45, 20);
            this.conDetParNup.TabIndex = 79;
            this.conDetParNup.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.conDetParNup.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.conDetParNup.Visible = false;
            this.conDetParNup.ValueChanged += new System.EventHandler(this.conDetParNup_ValueChanged);
            // 
            // conDetParLbox
            // 
            this.conDetParLbox.FormattingEnabled = true;
            this.conDetParLbox.Location = new System.Drawing.Point(514, 178);
            this.conDetParLbox.MultiColumn = true;
            this.conDetParLbox.Name = "conDetParLbox";
            this.conDetParLbox.Size = new System.Drawing.Size(87, 160);
            this.conDetParLbox.TabIndex = 78;
            this.conDetParLbox.Visible = false;
            this.conDetParLbox.SelectedIndexChanged += new System.EventHandler(this.conDetParLbox_SelectedIndexChanged);
            // 
            // conDetDetTb
            // 
            this.conDetDetTb.Location = new System.Drawing.Point(6, 178);
            this.conDetDetTb.Multiline = true;
            this.conDetDetTb.Name = "conDetDetTb";
            this.conDetDetTb.Size = new System.Drawing.Size(455, 136);
            this.conDetDetTb.TabIndex = 77;
            // 
            // conDetParTotTb
            // 
            this.conDetParTotTb.Enabled = false;
            this.conDetParTotTb.Location = new System.Drawing.Point(514, 350);
            this.conDetParTotTb.Name = "conDetParTotTb";
            this.conDetParTotTb.Size = new System.Drawing.Size(87, 20);
            this.conDetParTotTb.TabIndex = 77;
            this.conDetParTotTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.conDetParTotTb.Visible = false;
            // 
            // conDetParTb
            // 
            this.conDetParTb.Location = new System.Drawing.Point(514, 127);
            this.conDetParTb.Name = "conDetParTb";
            this.conDetParTb.Size = new System.Drawing.Size(87, 20);
            this.conDetParTb.TabIndex = 77;
            this.conDetParTb.Visible = false;
            this.conDetParTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conDetParTb_KeyDown);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel10.Controls.Add(this.conDataDtp);
            this.panel10.Controls.Add(this.conHoraCb);
            this.panel10.Controls.Add(this.label14);
            this.panel10.Controls.Add(this.label19);
            this.panel10.Controls.Add(this.conMinCb);
            this.panel10.Controls.Add(this.label11);
            this.panel10.Location = new System.Drawing.Point(0, 27);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(468, 81);
            this.panel10.TabIndex = 75;
            // 
            // conDataDtp
            // 
            this.conDataDtp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conDataDtp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.conDataDtp.Location = new System.Drawing.Point(117, 36);
            this.conDataDtp.Name = "conDataDtp";
            this.conDataDtp.Size = new System.Drawing.Size(119, 26);
            this.conDataDtp.TabIndex = 4;
            // 
            // conHoraCb
            // 
            this.conHoraCb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conHoraCb.FormattingEnabled = true;
            this.conHoraCb.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.conHoraCb.Location = new System.Drawing.Point(259, 36);
            this.conHoraCb.Name = "conHoraCb";
            this.conHoraCb.Size = new System.Drawing.Size(42, 28);
            this.conHoraCb.TabIndex = 74;
            this.conHoraCb.Text = "12";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(114, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Data";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(304, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Minutos";
            // 
            // conMinCb
            // 
            this.conMinCb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conMinCb.FormattingEnabled = true;
            this.conMinCb.Items.AddRange(new object[] {
            "00",
            "05",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55"});
            this.conMinCb.Location = new System.Drawing.Point(307, 36);
            this.conMinCb.Name = "conMinCb";
            this.conMinCb.Size = new System.Drawing.Size(42, 28);
            this.conMinCb.TabIndex = 74;
            this.conMinCb.Text = "00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(256, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Hora";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(3, 162);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(176, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Detalhes (Abordagem, metas, etc...)";
            // 
            // conDetAddLb
            // 
            this.conDetAddLb.AutoSize = true;
            this.conDetAddLb.Location = new System.Drawing.Point(400, 127);
            this.conDetAddLb.Name = "conDetAddLb";
            this.conDetAddLb.Size = new System.Drawing.Size(106, 13);
            this.conDetAddLb.TabIndex = 0;
            this.conDetAddLb.Text = "Inserir/Editar Parcela";
            this.conDetAddLb.Visible = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(512, 339);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(27, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "total";
            this.label55.Visible = false;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel12.Controls.Add(this.conDetFormaCb);
            this.panel12.Controls.Add(this.ConDetPgCh);
            this.panel12.Controls.Add(this.label57);
            this.panel12.Controls.Add(this.label56);
            this.panel12.Controls.Add(this.label53);
            this.panel12.Controls.Add(this.label24);
            this.panel12.Controls.Add(this.conDetValorTb);
            this.panel12.Controls.Add(this.conNovoBt);
            this.panel12.Location = new System.Drawing.Point(0, 321);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(466, 105);
            this.panel12.TabIndex = 75;
            // 
            // conDetFormaCb
            // 
            this.conDetFormaCb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conDetFormaCb.FormattingEnabled = true;
            this.conDetFormaCb.Items.AddRange(new object[] {
            "Dinheiro",
            "Debito",
            "Cheque",
            "CreditoAVista",
            "CreditoParcelado"});
            this.conDetFormaCb.Location = new System.Drawing.Point(187, 26);
            this.conDetFormaCb.Name = "conDetFormaCb";
            this.conDetFormaCb.Size = new System.Drawing.Size(193, 28);
            this.conDetFormaCb.TabIndex = 79;
            // 
            // ConDetPgCh
            // 
            this.ConDetPgCh.AutoSize = true;
            this.ConDetPgCh.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConDetPgCh.Location = new System.Drawing.Point(395, 27);
            this.ConDetPgCh.Name = "ConDetPgCh";
            this.ConDetPgCh.Size = new System.Drawing.Size(59, 29);
            this.ConDetPgCh.TabIndex = 78;
            this.ConDetPgCh.Text = "PG";
            this.ConDetPgCh.UseVisualStyleBackColor = true;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(381, 33);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(16, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "->";
            this.label57.Visible = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(171, 33);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(16, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "->";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(187, 10);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(108, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "Forma de Pagamento";
            this.label53.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(90, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Valor da Consulta";
            // 
            // conDetValorTb
            // 
            this.conDetValorTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conDetValorTb.Location = new System.Drawing.Point(9, 25);
            this.conDetValorTb.Name = "conDetValorTb";
            this.conDetValorTb.Size = new System.Drawing.Size(161, 30);
            this.conDetValorTb.TabIndex = 77;
            this.conDetValorTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.conDetValorTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conDetValorTb_KeyDown);
            // 
            // conNovoBt
            // 
            this.conNovoBt.Location = new System.Drawing.Point(6, 61);
            this.conNovoBt.Name = "conNovoBt";
            this.conNovoBt.Size = new System.Drawing.Size(448, 34);
            this.conNovoBt.TabIndex = 7;
            this.conNovoBt.Text = "Finalizar";
            this.conNovoBt.UseVisualStyleBackColor = true;
            this.conNovoBt.Click += new System.EventHandler(this.conSalvarBt_Click);
            // 
            // conNovaBt
            // 
            this.conNovaBt.Location = new System.Drawing.Point(18, 46);
            this.conNovaBt.Name = "conNovaBt";
            this.conNovaBt.Size = new System.Drawing.Size(93, 37);
            this.conNovaBt.TabIndex = 76;
            this.conNovaBt.Text = "Inserir nova Consulta";
            this.conNovaBt.UseVisualStyleBackColor = true;
            this.conNovaBt.Click += new System.EventHandler(this.conNovaBt_Click);
            // 
            // conPacienteLb
            // 
            this.conPacienteLb.AutoSize = true;
            this.conPacienteLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conPacienteLb.Location = new System.Drawing.Point(15, 6);
            this.conPacienteLb.Name = "conPacienteLb";
            this.conPacienteLb.Size = new System.Drawing.Size(124, 17);
            this.conPacienteLb.TabIndex = 3;
            this.conPacienteLb.Text = "Nome do Paciente";
            // 
            // conTodasLb
            // 
            this.conTodasLb.AutoSize = true;
            this.conTodasLb.Location = new System.Drawing.Point(533, 106);
            this.conTodasLb.Name = "conTodasLb";
            this.conTodasLb.Size = new System.Drawing.Size(55, 13);
            this.conTodasLb.TabIndex = 73;
            this.conTodasLb.Text = "ver Todas";
            this.conTodasLb.Click += new System.EventHandler(this.conTodasLb_Click);
            // 
            // agentaPn
            // 
            this.agentaPn.BackColor = System.Drawing.Color.LightBlue;
            this.agentaPn.Controls.Add(this.label18);
            this.agentaPn.Controls.Add(this.label17);
            this.agentaPn.Controls.Add(this.label31);
            this.agentaPn.Controls.Add(this.btnAnterior);
            this.agentaPn.Controls.Add(this.label36);
            this.agentaPn.Controls.Add(this.btnProximo);
            this.agentaPn.Controls.Add(this.label35);
            this.agentaPn.Controls.Add(this.label12);
            this.agentaPn.Controls.Add(this.label34);
            this.agentaPn.Controls.Add(this.panel1);
            this.agentaPn.Controls.Add(this.label33);
            this.agentaPn.Controls.Add(this.label32);
            this.agentaPn.Controls.Add(this.txtAno);
            this.agentaPn.Controls.Add(this.label37);
            this.agentaPn.Controls.Add(this.cboMes);
            this.agentaPn.Controls.Add(this.labelMes);
            this.agentaPn.Controls.Add(this.label13);
            this.agentaPn.Location = new System.Drawing.Point(615, 122);
            this.agentaPn.Name = "agentaPn";
            this.agentaPn.Size = new System.Drawing.Size(371, 318);
            this.agentaPn.TabIndex = 71;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(123, 262);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 17);
            this.label18.TabIndex = 60;
            this.label18.Text = "Consulta";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Green;
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(49, 262);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 17);
            this.label17.TabIndex = 60;
            this.label17.Text = "Hoje";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(49, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(37, 27);
            this.label31.TabIndex = 60;
            this.label31.Text = "Dom";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAnterior
            // 
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnterior.Location = new System.Drawing.Point(49, 22);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(26, 23);
            this.btnAnterior.TabIndex = 70;
            this.btnAnterior.Text = "<";
            this.btnAnterior.UseVisualStyleBackColor = false;
            this.btnAnterior.Click += new System.EventHandler(this.conCalPrev_Click);
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(92, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(37, 27);
            this.label36.TabIndex = 55;
            this.label36.Text = "Seg";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnProximo
            // 
            this.btnProximo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProximo.Location = new System.Drawing.Point(318, 22);
            this.btnProximo.Name = "btnProximo";
            this.btnProximo.Size = new System.Drawing.Size(26, 23);
            this.btnProximo.TabIndex = 69;
            this.btnProximo.Text = ">";
            this.btnProximo.UseVisualStyleBackColor = false;
            this.btnProximo.Click += new System.EventHandler(this.conCalProx_Click);
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(134, 48);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(37, 27);
            this.label35.TabIndex = 56;
            this.label35.Text = "Ter";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(194, 279);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 68;
            this.label12.Text = "Ano";
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(176, 48);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(37, 27);
            this.label34.TabIndex = 57;
            this.label34.Text = "Qua";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(49, 78);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(295, 171);
            this.panel1.TabIndex = 67;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(218, 48);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(37, 27);
            this.label33.TabIndex = 58;
            this.label33.Text = "Qui";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(260, 48);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(37, 27);
            this.label32.TabIndex = 59;
            this.label32.Text = "Sex";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAno
            // 
            this.txtAno.Location = new System.Drawing.Point(197, 295);
            this.txtAno.Mask = "0000";
            this.txtAno.Name = "txtAno";
            this.txtAno.Size = new System.Drawing.Size(45, 20);
            this.txtAno.TabIndex = 65;
            this.txtAno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAno_KeyDown);
            this.txtAno.Leave += new System.EventHandler(this.txtAno_Leave);
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(307, 48);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(37, 27);
            this.label37.TabIndex = 61;
            this.label37.Text = "Sab";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cboMes
            // 
            this.cboMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMes.FormattingEnabled = true;
            this.cboMes.ItemHeight = 13;
            this.cboMes.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.cboMes.Location = new System.Drawing.Point(134, 294);
            this.cboMes.Name = "cboMes";
            this.cboMes.Size = new System.Drawing.Size(57, 21);
            this.cboMes.TabIndex = 64;
            this.cboMes.SelectedIndexChanged += new System.EventHandler(this.cboMes_SelectedIndexChanged);
            // 
            // labelMes
            // 
            this.labelMes.AutoSize = true;
            this.labelMes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMes.Location = new System.Drawing.Point(173, 21);
            this.labelMes.Name = "labelMes";
            this.labelMes.Size = new System.Drawing.Size(47, 20);
            this.labelMes.TabIndex = 62;
            this.labelMes.Text = "MÊS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(146, 279);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 63;
            this.label13.Text = "Mês";
            // 
            // conLv
            // 
            this.conLv.AutoArrange = false;
            this.conLv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.conLv.FullRowSelect = true;
            this.conLv.GridLines = true;
            this.conLv.Location = new System.Drawing.Point(18, 121);
            this.conLv.MultiSelect = false;
            this.conLv.Name = "conLv";
            this.conLv.ShowItemToolTips = true;
            this.conLv.Size = new System.Drawing.Size(570, 320);
            this.conLv.TabIndex = 72;
            this.conLv.UseCompatibleStateImageBehavior = false;
            this.conLv.View = System.Windows.Forms.View.Details;
            this.conLv.SelectedIndexChanged += new System.EventHandler(this.conLv_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Consulta";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Paciente";
            this.columnHeader2.Width = 126;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Data";
            this.columnHeader3.Width = 171;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Pagou";
            this.columnHeader4.Width = 81;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Pendência";
            this.columnHeader5.Width = 65;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.LightBlue;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(726, 103);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(148, 20);
            this.label16.TabIndex = 3;
            this.label16.Text = "Proxima Consulta";
            // 
            // conPlanoAtualizaBt
            // 
            this.conPlanoAtualizaBt.Location = new System.Drawing.Point(299, 294);
            this.conPlanoAtualizaBt.Name = "conPlanoAtualizaBt";
            this.conPlanoAtualizaBt.Size = new System.Drawing.Size(75, 23);
            this.conPlanoAtualizaBt.TabIndex = 3;
            this.conPlanoAtualizaBt.Text = "Atualizar";
            this.conPlanoAtualizaBt.UseVisualStyleBackColor = true;
            this.conPlanoAtualizaBt.Click += new System.EventHandler(this.conPlanoAtualizaBt_Click);
            // 
            // conPlanoTb
            // 
            this.conPlanoTb.Location = new System.Drawing.Point(22, 28);
            this.conPlanoTb.Multiline = true;
            this.conPlanoTb.Name = "conPlanoTb";
            this.conPlanoTb.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.conPlanoTb.Size = new System.Drawing.Size(628, 262);
            this.conPlanoTb.TabIndex = 2;
            this.conPlanoTb.Leave += new System.EventHandler(this.conPlanoTb_Leave);
            // 
            // conParCb
            // 
            this.conParCb.FormattingEnabled = true;
            this.conParCb.Location = new System.Drawing.Point(303, 47);
            this.conParCb.Name = "conParCb";
            this.conParCb.Size = new System.Drawing.Size(121, 21);
            this.conParCb.TabIndex = 12;
            // 
            // conFinParDtp
            // 
            this.conFinParDtp.Location = new System.Drawing.Point(236, 130);
            this.conFinParDtp.Name = "conFinParDtp";
            this.conFinParDtp.Size = new System.Drawing.Size(219, 20);
            this.conFinParDtp.TabIndex = 16;
            // 
            // conFinChequeRb
            // 
            this.conFinChequeRb.AutoSize = true;
            this.conFinChequeRb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.conFinChequeRb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conFinChequeRb.Location = new System.Drawing.Point(523, 99);
            this.conFinChequeRb.Name = "conFinChequeRb";
            this.conFinChequeRb.Size = new System.Drawing.Size(77, 22);
            this.conFinChequeRb.TabIndex = 13;
            this.conFinChequeRb.TabStop = true;
            this.conFinChequeRb.Text = "Cheque";
            this.conFinChequeRb.UseVisualStyleBackColor = false;
            // 
            // conFinCartRb
            // 
            this.conFinCartRb.AutoSize = true;
            this.conFinCartRb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.conFinCartRb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conFinCartRb.Location = new System.Drawing.Point(523, 130);
            this.conFinCartRb.Name = "conFinCartRb";
            this.conFinCartRb.Size = new System.Drawing.Size(71, 22);
            this.conFinCartRb.TabIndex = 13;
            this.conFinCartRb.TabStop = true;
            this.conFinCartRb.Text = "Cartão";
            this.conFinCartRb.UseVisualStyleBackColor = false;
            // 
            // conFinDinRb
            // 
            this.conFinDinRb.AutoSize = true;
            this.conFinDinRb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.conFinDinRb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conFinDinRb.Location = new System.Drawing.Point(523, 69);
            this.conFinDinRb.Name = "conFinDinRb";
            this.conFinDinRb.Size = new System.Drawing.Size(81, 22);
            this.conFinDinRb.TabIndex = 13;
            this.conFinDinRb.TabStop = true;
            this.conFinDinRb.Text = "Dinheiro";
            this.conFinDinRb.UseVisualStyleBackColor = false;
            // 
            // conFinValorTb
            // 
            this.conFinValorTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conFinValorTb.Location = new System.Drawing.Point(267, 82);
            this.conFinValorTb.Name = "conFinValorTb";
            this.conFinValorTb.Size = new System.Drawing.Size(157, 26);
            this.conFinValorTb.TabIndex = 12;
            this.conFinValorTb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.conFinValorTb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinValorTb_KeyDown);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(486, 44);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(163, 20);
            this.label22.TabIndex = 11;
            this.label22.Text = "Forma de Pagamento";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(832, 204);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 13);
            this.label20.TabIndex = 11;
            this.label20.Text = "mostrar Todas";
            // 
            // conFinLv
            // 
            this.conFinLv.BackColor = System.Drawing.Color.LightSteelBlue;
            this.conFinLv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.conFinLv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.conFinNomeCol,
            this.conFinDataCol,
            this.conFinParCol,
            this.conFinFormaCol,
            this.conFinPendenciaCol});
            this.conFinLv.Location = new System.Drawing.Point(3, 219);
            this.conFinLv.Name = "conFinLv";
            this.conFinLv.Size = new System.Drawing.Size(893, 340);
            this.conFinLv.TabIndex = 10;
            this.conFinLv.UseCompatibleStateImageBehavior = false;
            // 
            // conFinNomeCol
            // 
            this.conFinNomeCol.Text = "Nome";
            // 
            // conFinDataCol
            // 
            this.conFinDataCol.Text = "Data";
            // 
            // conFinParCol
            // 
            this.conFinParCol.Text = "Parcela";
            // 
            // conFinFormaCol
            // 
            this.conFinFormaCol.DisplayIndex = 4;
            this.conFinFormaCol.Text = "Forma de Pagamento";
            // 
            // conFinPendenciaCol
            // 
            this.conFinPendenciaCol.DisplayIndex = 3;
            this.conFinPendenciaCol.Text = "Pendência";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.conFinVezesNup);
            this.panel5.Controls.Add(this.conFinParRb);
            this.panel5.Controls.Add(this.conFinCredVistaRb);
            this.panel5.Controls.Add(this.conFinDebRb);
            this.panel5.Controls.Add(this.conFinAddOpLb);
            this.panel5.Controls.Add(this.conTaxNameLb);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Location = new System.Drawing.Point(680, 39);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(226, 125);
            this.panel5.TabIndex = 15;
            // 
            // conFinVezesNup
            // 
            this.conFinVezesNup.Location = new System.Drawing.Point(118, 86);
            this.conFinVezesNup.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.conFinVezesNup.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.conFinVezesNup.Name = "conFinVezesNup";
            this.conFinVezesNup.Size = new System.Drawing.Size(39, 20);
            this.conFinVezesNup.TabIndex = 17;
            this.conFinVezesNup.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // conFinParRb
            // 
            this.conFinParRb.AutoSize = true;
            this.conFinParRb.Location = new System.Drawing.Point(11, 86);
            this.conFinParRb.Name = "conFinParRb";
            this.conFinParRb.Size = new System.Drawing.Size(109, 17);
            this.conFinParRb.TabIndex = 16;
            this.conFinParRb.TabStop = true;
            this.conFinParRb.Text = "Crédito Parcelado";
            this.conFinParRb.UseVisualStyleBackColor = true;
            // 
            // conFinCredVistaRb
            // 
            this.conFinCredVistaRb.AutoSize = true;
            this.conFinCredVistaRb.Location = new System.Drawing.Point(11, 63);
            this.conFinCredVistaRb.Name = "conFinCredVistaRb";
            this.conFinCredVistaRb.Size = new System.Drawing.Size(92, 17);
            this.conFinCredVistaRb.TabIndex = 16;
            this.conFinCredVistaRb.TabStop = true;
            this.conFinCredVistaRb.Text = "Crédito à vista";
            this.conFinCredVistaRb.UseVisualStyleBackColor = true;
            // 
            // conFinDebRb
            // 
            this.conFinDebRb.AutoSize = true;
            this.conFinDebRb.Location = new System.Drawing.Point(11, 41);
            this.conFinDebRb.Name = "conFinDebRb";
            this.conFinDebRb.Size = new System.Drawing.Size(56, 17);
            this.conFinDebRb.TabIndex = 16;
            this.conFinDebRb.TabStop = true;
            this.conFinDebRb.Text = "Débito";
            this.conFinDebRb.UseVisualStyleBackColor = true;
            // 
            // conFinAddOpLb
            // 
            this.conFinAddOpLb.AutoSize = true;
            this.conFinAddOpLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conFinAddOpLb.Location = new System.Drawing.Point(144, 22);
            this.conFinAddOpLb.Name = "conFinAddOpLb";
            this.conFinAddOpLb.Size = new System.Drawing.Size(80, 13);
            this.conFinAddOpLb.TabIndex = 15;
            this.conFinAddOpLb.Text = "Adicionar/Editar";
            this.conFinAddOpLb.Click += new System.EventHandler(this.conFinAddOpLb_Click);
            // 
            // conTaxNameLb
            // 
            this.conTaxNameLb.AutoSize = true;
            this.conTaxNameLb.BackColor = System.Drawing.Color.Transparent;
            this.conTaxNameLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conTaxNameLb.Location = new System.Drawing.Point(3, 17);
            this.conTaxNameLb.Name = "conTaxNameLb";
            this.conTaxNameLb.Size = new System.Drawing.Size(85, 20);
            this.conTaxNameLb.TabIndex = 11;
            this.conTaxNameLb.Text = "Operadora";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(155, 85);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(45, 17);
            this.label28.TabIndex = 11;
            this.label28.Text = "vezes";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.panel4.Location = new System.Drawing.Point(460, 39);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(226, 125);
            this.panel4.TabIndex = 15;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.conFinConsultaLb);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.conFinPacienteLb);
            this.panel2.Location = new System.Drawing.Point(8, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(223, 125);
            this.panel2.TabIndex = 15;
            // 
            // conFinConsultaLb
            // 
            this.conFinConsultaLb.AutoSize = true;
            this.conFinConsultaLb.Location = new System.Drawing.Point(3, 84);
            this.conFinConsultaLb.Name = "conFinConsultaLb";
            this.conFinConsultaLb.Size = new System.Drawing.Size(48, 13);
            this.conFinConsultaLb.TabIndex = 17;
            this.conFinConsultaLb.Text = "Consulta";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(2, 63);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(58, 13);
            this.label25.TabIndex = 17;
            this.label25.Text = "referente a";
            // 
            // conFinPacienteLb
            // 
            this.conFinPacienteLb.AutoSize = true;
            this.conFinPacienteLb.Location = new System.Drawing.Point(2, 36);
            this.conFinPacienteLb.Name = "conFinPacienteLb";
            this.conFinPacienteLb.Size = new System.Drawing.Size(55, 13);
            this.conFinPacienteLb.TabIndex = 17;
            this.conFinPacienteLb.Text = "Paciente: ";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Location = new System.Drawing.Point(229, 39);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(245, 125);
            this.panel3.TabIndex = 15;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(7, 46);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 20);
            this.label21.TabIndex = 11;
            this.label21.Text = "R$";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(5, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(62, 20);
            this.label23.TabIndex = 11;
            this.label23.Text = "Parcela";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel8.Controls.Add(this.conSalvaLb);
            this.panel8.Location = new System.Drawing.Point(462, 181);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(424, 26);
            this.panel8.TabIndex = 15;
            // 
            // conSalvaLb
            // 
            this.conSalvaLb.AutoSize = true;
            this.conSalvaLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conSalvaLb.ForeColor = System.Drawing.SystemColors.ControlText;
            this.conSalvaLb.Location = new System.Drawing.Point(120, 0);
            this.conSalvaLb.Name = "conSalvaLb";
            this.conSalvaLb.Size = new System.Drawing.Size(170, 25);
            this.conSalvaLb.TabIndex = 17;
            this.conSalvaLb.Text = "Inserir Pagamento";
            this.conSalvaLb.Click += new System.EventHandler(this.conFaturaBt_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.panel7.Controls.Add(this.conLimparLb);
            this.panel7.Location = new System.Drawing.Point(7, 181);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(429, 26);
            this.panel7.TabIndex = 15;
            // 
            // conLimparLb
            // 
            this.conLimparLb.AutoSize = true;
            this.conLimparLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conLimparLb.ForeColor = System.Drawing.SystemColors.ControlText;
            this.conLimparLb.Location = new System.Drawing.Point(153, 0);
            this.conLimparLb.Name = "conLimparLb";
            this.conLimparLb.Size = new System.Drawing.Size(130, 25);
            this.conLimparLb.TabIndex = 17;
            this.conLimparLb.Text = "Limpar dados";
            this.conLimparLb.Click += new System.EventHandler(this.conLimparLb_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label27.Location = new System.Drawing.Point(363, 7);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(148, 26);
            this.label27.TabIndex = 17;
            this.label27.Text = "FINANCEIRO";
            // 
            // conOperPn
            // 
            this.conOperPn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.conOperPn.Controls.Add(this.conTaxNameCb);
            this.conOperPn.Controls.Add(this.conTaxEsteCh);
            this.conOperPn.Controls.Add(this.conTaxSaveBt);
            this.conOperPn.Controls.Add(this.label51);
            this.conOperPn.Controls.Add(this.conTaxName2Tb);
            this.conOperPn.Controls.Add(this.label29);
            this.conOperPn.Controls.Add(this.conTaxPn);
            this.conOperPn.Location = new System.Drawing.Point(747, 56);
            this.conOperPn.Name = "conOperPn";
            this.conOperPn.Size = new System.Drawing.Size(171, 472);
            this.conOperPn.TabIndex = 15;
            this.conOperPn.Visible = false;
            // 
            // conTaxNameCb
            // 
            this.conTaxNameCb.FormattingEnabled = true;
            this.conTaxNameCb.Location = new System.Drawing.Point(6, 34);
            this.conTaxNameCb.Name = "conTaxNameCb";
            this.conTaxNameCb.Size = new System.Drawing.Size(153, 21);
            this.conTaxNameCb.TabIndex = 18;
            this.conTaxNameCb.SelectedIndexChanged += new System.EventHandler(this.conTaxNameCb_SelectedIndexChanged);
            this.conTaxNameCb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conTaxNameCb_KeyDown);
            // 
            // conTaxEsteCh
            // 
            this.conTaxEsteCh.AutoSize = true;
            this.conTaxEsteCh.Location = new System.Drawing.Point(6, 88);
            this.conTaxEsteCh.Name = "conTaxEsteCh";
            this.conTaxEsteCh.Size = new System.Drawing.Size(146, 17);
            this.conTaxEsteCh.TabIndex = 17;
            this.conTaxEsteCh.Text = "Utilizar este como Padrão";
            this.conTaxEsteCh.UseVisualStyleBackColor = true;
            // 
            // conTaxSaveBt
            // 
            this.conTaxSaveBt.Location = new System.Drawing.Point(6, 444);
            this.conTaxSaveBt.Name = "conTaxSaveBt";
            this.conTaxSaveBt.Size = new System.Drawing.Size(160, 23);
            this.conTaxSaveBt.TabIndex = 16;
            this.conTaxSaveBt.Text = "Salvar";
            this.conTaxSaveBt.UseVisualStyleBackColor = true;
            this.conTaxSaveBt.Click += new System.EventHandler(this.conTaxSaveBt_Click);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(155, 5);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(14, 13);
            this.label51.TabIndex = 11;
            this.label51.Text = "X";
            this.label51.Click += new System.EventHandler(this.label51_Click);
            // 
            // conTaxName2Tb
            // 
            this.conTaxName2Tb.AutoSize = true;
            this.conTaxName2Tb.Location = new System.Drawing.Point(3, 62);
            this.conTaxName2Tb.Name = "conTaxName2Tb";
            this.conTaxName2Tb.Size = new System.Drawing.Size(57, 13);
            this.conTaxName2Tb.TabIndex = 11;
            this.conTaxName2Tb.Text = "Operadora";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(1, 17);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(106, 13);
            this.label29.TabIndex = 11;
            this.label29.Text = "Nome da Operadora:";
            // 
            // conTaxPn
            // 
            this.conTaxPn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.conTaxPn.Controls.Add(this.conTx12Tb);
            this.conTaxPn.Controls.Add(this.conTx9Tb);
            this.conTaxPn.Controls.Add(this.label50);
            this.conTaxPn.Controls.Add(this.conTx5Tb);
            this.conTaxPn.Controls.Add(this.label47);
            this.conTaxPn.Controls.Add(this.conTx10Tb);
            this.conTaxPn.Controls.Add(this.label43);
            this.conTaxPn.Controls.Add(this.conTx11Tb);
            this.conTaxPn.Controls.Add(this.conTx7Tb);
            this.conTaxPn.Controls.Add(this.conTx8Tb);
            this.conTaxPn.Controls.Add(this.label49);
            this.conTaxPn.Controls.Add(this.conTx3Tb);
            this.conTaxPn.Controls.Add(this.label46);
            this.conTaxPn.Controls.Add(this.label48);
            this.conTaxPn.Controls.Add(this.conTx4Tb);
            this.conTaxPn.Controls.Add(this.label45);
            this.conTaxPn.Controls.Add(this.label41);
            this.conTaxPn.Controls.Add(this.conTx6Tb);
            this.conTaxPn.Controls.Add(this.label42);
            this.conTaxPn.Controls.Add(this.label44);
            this.conTaxPn.Controls.Add(this.conTx2Tb);
            this.conTaxPn.Controls.Add(this.label40);
            this.conTaxPn.Controls.Add(this.conTx1Tb);
            this.conTaxPn.Controls.Add(this.label39);
            this.conTaxPn.Controls.Add(this.conTx0Tb);
            this.conTaxPn.Controls.Add(this.label30);
            this.conTaxPn.Controls.Add(this.label38);
            this.conTaxPn.Location = new System.Drawing.Point(6, 115);
            this.conTaxPn.Name = "conTaxPn";
            this.conTaxPn.Size = new System.Drawing.Size(160, 323);
            this.conTaxPn.TabIndex = 15;
            // 
            // conTx12Tb
            // 
            this.conTx12Tb.Location = new System.Drawing.Point(77, 296);
            this.conTx12Tb.Name = "conTx12Tb";
            this.conTx12Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx12Tb.TabIndex = 13;
            this.conTx12Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // conTx9Tb
            // 
            this.conTx9Tb.Location = new System.Drawing.Point(77, 227);
            this.conTx9Tb.Name = "conTx9Tb";
            this.conTx9Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx9Tb.TabIndex = 13;
            this.conTx9Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(3, 299);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(60, 13);
            this.label50.TabIndex = 11;
            this.label50.Text = "Crédito 12x";
            // 
            // conTx5Tb
            // 
            this.conTx5Tb.Location = new System.Drawing.Point(77, 135);
            this.conTx5Tb.Name = "conTx5Tb";
            this.conTx5Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx5Tb.TabIndex = 13;
            this.conTx5Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(3, 230);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(54, 13);
            this.label47.TabIndex = 11;
            this.label47.Text = "Crédito 9x";
            // 
            // conTx10Tb
            // 
            this.conTx10Tb.Location = new System.Drawing.Point(77, 250);
            this.conTx10Tb.Name = "conTx10Tb";
            this.conTx10Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx10Tb.TabIndex = 13;
            this.conTx10Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(3, 138);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 13);
            this.label43.TabIndex = 11;
            this.label43.Text = "Crédito 5x";
            // 
            // conTx11Tb
            // 
            this.conTx11Tb.Location = new System.Drawing.Point(77, 273);
            this.conTx11Tb.Name = "conTx11Tb";
            this.conTx11Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx11Tb.TabIndex = 13;
            this.conTx11Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // conTx7Tb
            // 
            this.conTx7Tb.Location = new System.Drawing.Point(77, 181);
            this.conTx7Tb.Name = "conTx7Tb";
            this.conTx7Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx7Tb.TabIndex = 13;
            this.conTx7Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // conTx8Tb
            // 
            this.conTx8Tb.Location = new System.Drawing.Point(77, 204);
            this.conTx8Tb.Name = "conTx8Tb";
            this.conTx8Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx8Tb.TabIndex = 13;
            this.conTx8Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(3, 253);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(60, 13);
            this.label49.TabIndex = 11;
            this.label49.Text = "Crédito 10x";
            // 
            // conTx3Tb
            // 
            this.conTx3Tb.Location = new System.Drawing.Point(77, 89);
            this.conTx3Tb.Name = "conTx3Tb";
            this.conTx3Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx3Tb.TabIndex = 13;
            this.conTx3Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(3, 184);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(54, 13);
            this.label46.TabIndex = 11;
            this.label46.Text = "Crédito 7x";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(3, 276);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(60, 13);
            this.label48.TabIndex = 11;
            this.label48.Text = "Crédito 11x";
            // 
            // conTx4Tb
            // 
            this.conTx4Tb.Location = new System.Drawing.Point(77, 112);
            this.conTx4Tb.Name = "conTx4Tb";
            this.conTx4Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx4Tb.TabIndex = 13;
            this.conTx4Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(3, 207);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 13);
            this.label45.TabIndex = 11;
            this.label45.Text = "Crédito 8x";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(3, 92);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(54, 13);
            this.label41.TabIndex = 11;
            this.label41.Text = "Crédito 3x";
            // 
            // conTx6Tb
            // 
            this.conTx6Tb.Location = new System.Drawing.Point(77, 158);
            this.conTx6Tb.Name = "conTx6Tb";
            this.conTx6Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx6Tb.TabIndex = 13;
            this.conTx6Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(3, 115);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(54, 13);
            this.label42.TabIndex = 11;
            this.label42.Text = "Crédito 4x";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(3, 161);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(54, 13);
            this.label44.TabIndex = 11;
            this.label44.Text = "Crédito 6x";
            // 
            // conTx2Tb
            // 
            this.conTx2Tb.Location = new System.Drawing.Point(77, 66);
            this.conTx2Tb.Name = "conTx2Tb";
            this.conTx2Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx2Tb.TabIndex = 13;
            this.conTx2Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(3, 69);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 13);
            this.label40.TabIndex = 11;
            this.label40.Text = "Crédito 2x";
            // 
            // conTx1Tb
            // 
            this.conTx1Tb.Location = new System.Drawing.Point(77, 43);
            this.conTx1Tb.Name = "conTx1Tb";
            this.conTx1Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx1Tb.TabIndex = 13;
            this.conTx1Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(3, 46);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(75, 13);
            this.label39.TabIndex = 11;
            this.label39.Text = "Crédito a Vista";
            // 
            // conTx0Tb
            // 
            this.conTx0Tb.Location = new System.Drawing.Point(77, 20);
            this.conTx0Tb.Name = "conTx0Tb";
            this.conTx0Tb.Size = new System.Drawing.Size(70, 20);
            this.conTx0Tb.TabIndex = 13;
            this.conTx0Tb.KeyDown += new System.Windows.Forms.KeyEventHandler(this.conFinTaxesTb_KeyDown);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(3, 23);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(38, 13);
            this.label30.TabIndex = 11;
            this.label30.Text = "Débito";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(84, 2);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(51, 20);
            this.label38.TabIndex = 11;
            this.label38.Text = "Taxas";
            // 
            // cad1Pn
            // 
            this.cad1Pn.BackColor = System.Drawing.Color.LightGray;
            this.cad1Pn.Controls.Add(this.cad1ConsultasBt);
            this.cad1Pn.Controls.Add(this.cad1AntLb);
            this.cad1Pn.Controls.Add(this.cad1DeletaBt);
            this.cad1Pn.Controls.Add(this.cad1LimpaLb);
            this.cad1Pn.Controls.Add(this.cad1XLb);
            this.cad1Pn.Controls.Add(this.cad1ProxLv);
            this.cad1Pn.Controls.Add(this.cad1NovoBt);
            this.cad1Pn.Controls.Add(this.cadNascDtp);
            this.cad1Pn.Controls.Add(this.cad1AtualizaBt);
            this.cad1Pn.Controls.Add(this.cad1IdMtb);
            this.cad1Pn.Controls.Add(this.label3);
            this.cad1Pn.Controls.Add(this.cad1NomeTb);
            this.cad1Pn.Controls.Add(this.label1);
            this.cad1Pn.Controls.Add(this.cad1UfTb);
            this.cad1Pn.Controls.Add(this.cad1RgMtb);
            this.cad1Pn.Controls.Add(this.label6);
            this.cad1Pn.Controls.Add(this.label4);
            this.cad1Pn.Controls.Add(this.cad1CidadeTb);
            this.cad1Pn.Controls.Add(this.cad1EnderecoTb);
            this.cad1Pn.Controls.Add(this.label2);
            this.cad1Pn.Controls.Add(this.cad1CpfMtb);
            this.cad1Pn.Controls.Add(this.label10);
            this.cad1Pn.Controls.Add(this.label7);
            this.cad1Pn.Controls.Add(this.label9);
            this.cad1Pn.Controls.Add(this.label8);
            this.cad1Pn.Controls.Add(this.cad1ObsTb);
            this.cad1Pn.Controls.Add(this.label5);
            this.cad1Pn.Controls.Add(this.cad1BairroTb);
            this.cad1Pn.Location = new System.Drawing.Point(249, 30);
            this.cad1Pn.Name = "cad1Pn";
            this.cad1Pn.Size = new System.Drawing.Size(627, 389);
            this.cad1Pn.TabIndex = 4;
            this.cad1Pn.Visible = false;
            this.cad1Pn.VisibleChanged += new System.EventHandler(this.cad1Pn_VisibleChanged);
            this.cad1Pn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.control_MouseDown);
            this.cad1Pn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.control_MouseMove);
            this.cad1Pn.MouseUp += new System.Windows.Forms.MouseEventHandler(this.control_MouseUp);
            // 
            // cad1ConsultasBt
            // 
            this.cad1ConsultasBt.Location = new System.Drawing.Point(22, 345);
            this.cad1ConsultasBt.Name = "cad1ConsultasBt";
            this.cad1ConsultasBt.Size = new System.Drawing.Size(587, 31);
            this.cad1ConsultasBt.TabIndex = 5;
            this.cad1ConsultasBt.Text = "Consultas";
            this.cad1ConsultasBt.UseVisualStyleBackColor = true;
            this.cad1ConsultasBt.Click += new System.EventHandler(this.cad1ConsultasBt_Click);
            // 
            // cad1AntLb
            // 
            this.cad1AntLb.AutoSize = true;
            this.cad1AntLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1AntLb.Location = new System.Drawing.Point(1, 37);
            this.cad1AntLb.Name = "cad1AntLb";
            this.cad1AntLb.Size = new System.Drawing.Size(19, 20);
            this.cad1AntLb.TabIndex = 4;
            this.cad1AntLb.Text = "<";
            this.cad1AntLb.Click += new System.EventHandler(this.cad1AntLb_Click);
            // 
            // cad1LimpaLb
            // 
            this.cad1LimpaLb.AutoSize = true;
            this.cad1LimpaLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1LimpaLb.Location = new System.Drawing.Point(583, 6);
            this.cad1LimpaLb.Name = "cad1LimpaLb";
            this.cad1LimpaLb.Size = new System.Drawing.Size(21, 15);
            this.cad1LimpaLb.TabIndex = 4;
            this.cad1LimpaLb.Text = "Lp";
            this.cad1LimpaLb.Click += new System.EventHandler(this.cad1LimpaLb_Click);
            // 
            // cad1XLb
            // 
            this.cad1XLb.AutoSize = true;
            this.cad1XLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1XLb.Location = new System.Drawing.Point(604, 4);
            this.cad1XLb.Name = "cad1XLb";
            this.cad1XLb.Size = new System.Drawing.Size(21, 20);
            this.cad1XLb.TabIndex = 4;
            this.cad1XLb.Text = "X";
            this.cad1XLb.Click += new System.EventHandler(this.cad1XLb_Click);
            // 
            // cad1ProxLv
            // 
            this.cad1ProxLv.AutoSize = true;
            this.cad1ProxLv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cad1ProxLv.Location = new System.Drawing.Point(55, 37);
            this.cad1ProxLv.Name = "cad1ProxLv";
            this.cad1ProxLv.Size = new System.Drawing.Size(19, 20);
            this.cad1ProxLv.TabIndex = 4;
            this.cad1ProxLv.Text = ">";
            this.cad1ProxLv.Click += new System.EventHandler(this.cad1ProxLv_Click);
            // 
            // cadNascDtp
            // 
            this.cadNascDtp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cadNascDtp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.cadNascDtp.Location = new System.Drawing.Point(404, 34);
            this.cadNascDtp.Name = "cadNascDtp";
            this.cadNascDtp.Size = new System.Drawing.Size(100, 26);
            this.cadNascDtp.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(401, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Data de Nascimento";
            // 
            // consultasPn
            // 
            this.consultasPn.BackColor = System.Drawing.Color.PowderBlue;
            this.consultasPn.Controls.Add(this.conDetailsPn);
            this.consultasPn.Controls.Add(this.conPacienteLb);
            this.consultasPn.Controls.Add(this.conNovaBt);
            this.consultasPn.Controls.Add(this.label16);
            this.consultasPn.Controls.Add(this.conLv);
            this.consultasPn.Controls.Add(this.conTodasLb);
            this.consultasPn.Controls.Add(this.agentaPn);
            this.consultasPn.Location = new System.Drawing.Point(40, 24);
            this.consultasPn.Name = "consultasPn";
            this.consultasPn.Size = new System.Drawing.Size(1015, 500);
            this.consultasPn.TabIndex = 77;
            this.consultasPn.Visible = false;
            this.consultasPn.VisibleChanged += new System.EventHandler(this.consultasPn_VisibleChanged);
            // 
            // finPn
            // 
            this.finPn.BackColor = System.Drawing.Color.LightSteelBlue;
            this.finPn.Controls.Add(this.conOperPn);
            this.finPn.Controls.Add(this.finLpLb);
            this.finPn.Controls.Add(this.finXLb);
            this.finPn.Controls.Add(this.panel8);
            this.finPn.Controls.Add(this.panel7);
            this.finPn.Controls.Add(this.conParCb);
            this.finPn.Controls.Add(this.label27);
            this.finPn.Controls.Add(this.conFinParDtp);
            this.finPn.Controls.Add(this.conFinChequeRb);
            this.finPn.Controls.Add(this.panel3);
            this.finPn.Controls.Add(this.conFinCartRb);
            this.finPn.Controls.Add(this.panel2);
            this.finPn.Controls.Add(this.conFinDinRb);
            this.finPn.Controls.Add(this.panel4);
            this.finPn.Controls.Add(this.conFinValorTb);
            this.finPn.Controls.Add(this.panel5);
            this.finPn.Controls.Add(this.label22);
            this.finPn.Controls.Add(this.conFinLv);
            this.finPn.Controls.Add(this.label20);
            this.finPn.Location = new System.Drawing.Point(67, 38);
            this.finPn.Name = "finPn";
            this.finPn.Size = new System.Drawing.Size(909, 562);
            this.finPn.TabIndex = 78;
            this.finPn.Visible = false;
            // 
            // finLpLb
            // 
            this.finLpLb.AutoSize = true;
            this.finLpLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finLpLb.Location = new System.Drawing.Point(862, 10);
            this.finLpLb.Name = "finLpLb";
            this.finLpLb.Size = new System.Drawing.Size(21, 15);
            this.finLpLb.TabIndex = 17;
            this.finLpLb.Text = "Lp";
            this.finLpLb.Click += new System.EventHandler(this.finLpLb_Click);
            // 
            // finXLb
            // 
            this.finXLb.AutoSize = true;
            this.finXLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finXLb.Location = new System.Drawing.Point(886, 9);
            this.finXLb.Name = "finXLb";
            this.finXLb.Size = new System.Drawing.Size(18, 17);
            this.finXLb.TabIndex = 17;
            this.finXLb.Text = "X";
            this.finXLb.Click += new System.EventHandler(this.finXLb_Click);
            // 
            // planoPn
            // 
            this.planoPn.BackColor = System.Drawing.Color.LightGray;
            this.planoPn.Controls.Add(this.planoXLb);
            this.planoPn.Controls.Add(this.label58);
            this.planoPn.Controls.Add(this.conPlanoAtualizaBt);
            this.planoPn.Controls.Add(this.conPlanoTb);
            this.planoPn.Controls.Add(this.planoNomeLb);
            this.planoPn.Location = new System.Drawing.Point(314, 12);
            this.planoPn.Name = "planoPn";
            this.planoPn.Size = new System.Drawing.Size(669, 323);
            this.planoPn.TabIndex = 4;
            this.planoPn.Visible = false;
            // 
            // planoXLb
            // 
            this.planoXLb.AutoSize = true;
            this.planoXLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.planoXLb.Location = new System.Drawing.Point(632, 5);
            this.planoXLb.Name = "planoXLb";
            this.planoXLb.Size = new System.Drawing.Size(18, 17);
            this.planoXLb.TabIndex = 4;
            this.planoXLb.Text = "X";
            this.planoXLb.Click += new System.EventHandler(this.planoXLb_Click);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(253, 6);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(166, 20);
            this.label58.TabIndex = 4;
            this.label58.Text = "Plano de Atendimento";
            // 
            // planoNomeLb
            // 
            this.planoNomeLb.AutoSize = true;
            this.planoNomeLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.planoNomeLb.Location = new System.Drawing.Point(19, 12);
            this.planoNomeLb.Name = "planoNomeLb";
            this.planoNomeLb.Size = new System.Drawing.Size(35, 13);
            this.planoNomeLb.TabIndex = 0;
            this.planoNomeLb.Text = "Nome";
            // 
            // avalPn
            // 
            this.avalPn.BackColor = System.Drawing.Color.LightGray;
            this.avalPn.Controls.Add(this.avalXLb);
            this.avalPn.Controls.Add(this.label59);
            this.avalPn.Controls.Add(this.conAvalTb);
            this.avalPn.Controls.Add(this.avalNomeLb);
            this.avalPn.Location = new System.Drawing.Point(40, 50);
            this.avalPn.Name = "avalPn";
            this.avalPn.Size = new System.Drawing.Size(621, 423);
            this.avalPn.TabIndex = 3;
            this.avalPn.Visible = false;
            // 
            // avalXLb
            // 
            this.avalXLb.AutoSize = true;
            this.avalXLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avalXLb.Location = new System.Drawing.Point(599, 2);
            this.avalXLb.Name = "avalXLb";
            this.avalXLb.Size = new System.Drawing.Size(18, 17);
            this.avalXLb.TabIndex = 4;
            this.avalXLb.Text = "X";
            this.avalXLb.Click += new System.EventHandler(this.avalXLb_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(279, 4);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(77, 20);
            this.label59.TabIndex = 4;
            this.label59.Text = "Avaliação";
            // 
            // avalNomeLb
            // 
            this.avalNomeLb.AutoSize = true;
            this.avalNomeLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avalNomeLb.Location = new System.Drawing.Point(14, 11);
            this.avalNomeLb.Name = "avalNomeLb";
            this.avalNomeLb.Size = new System.Drawing.Size(35, 13);
            this.avalNomeLb.TabIndex = 0;
            this.avalNomeLb.Text = "Nome";
            // 
            // fichaEvolutivaToolStripMenuItem1
            // 
            this.fichaEvolutivaToolStripMenuItem1.Name = "fichaEvolutivaToolStripMenuItem1";
            this.fichaEvolutivaToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.fichaEvolutivaToolStripMenuItem1.Text = "Ficha Evolutiva";
            // 
            // principalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 662);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.consultasPn);
            this.Controls.Add(this.finPn);
            this.Controls.Add(this.cad1Pn);
            this.Controls.Add(this.avalPn);
            this.Controls.Add(this.planoPn);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "principalForm";
            this.Text = "AnamSys";
            this.Load += new System.EventHandler(this.principalForm_Load);
            this.Resize += new System.EventHandler(this.principalForm_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.conDetailsPn.ResumeLayout(false);
            this.conDetailsPn.PerformLayout();
            this.conDetParPn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.conDetParNup)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.agentaPn.ResumeLayout(false);
            this.agentaPn.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.conFinVezesNup)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.conOperPn.ResumeLayout(false);
            this.conOperPn.PerformLayout();
            this.conTaxPn.ResumeLayout(false);
            this.conTaxPn.PerformLayout();
            this.cad1Pn.ResumeLayout(false);
            this.cad1Pn.PerformLayout();
            this.consultasPn.ResumeLayout(false);
            this.consultasPn.PerformLayout();
            this.finPn.ResumeLayout(false);
            this.finPn.PerformLayout();
            this.planoPn.ResumeLayout(false);
            this.planoPn.PerformLayout();
            this.avalPn.ResumeLayout(false);
            this.avalPn.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem anamneseToolStripMenuItem;
        private System.Windows.Forms.TextBox cad1UfTb;
        private System.Windows.Forms.TextBox cad1CidadeTb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox cad1BairroTb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox cad1EnderecoTb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox cad1NomeTb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox cad1IdMtb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox cad1RgMtb;
        private System.Windows.Forms.MaskedTextBox cad1CpfMtb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox cad1ObsTb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button cad1DeletaBt;
        private System.Windows.Forms.Button cad1NovoBt;
        private System.Windows.Forms.Button cad1AtualizaBt;
        private System.Windows.Forms.ToolStripMenuItem anamneseToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem faturarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agendaToolStripMenuItem;
        private System.Windows.Forms.TextBox conAvalTb;
        private System.Windows.Forms.DateTimePicker conDataDtp;
        private System.Windows.Forms.Label conPacienteLb;
        private System.Windows.Forms.TextBox conPlanoTb;
        private System.Windows.Forms.ToolStripMenuItem fichaEvolutivaToolStripMenuItem;
        private System.Windows.Forms.Button conNovoBt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnProximo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MaskedTextBox txtAno;
        private System.Windows.Forms.ComboBox cboMes;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelMes;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel agentaPn;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ListView conLv;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label conTodasLb;
        private System.Windows.Forms.ComboBox conMinCb;
        private System.Windows.Forms.ComboBox conHoraCb;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListView conFinLv;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ColumnHeader conFinNomeCol;
        private System.Windows.Forms.ColumnHeader conFinDataCol;
        private System.Windows.Forms.ColumnHeader conFinParCol;
        private System.Windows.Forms.ColumnHeader conFinPendenciaCol;
        private System.Windows.Forms.ColumnHeader conFinFormaCol;
        private System.Windows.Forms.TextBox conFinValorTb;
        private System.Windows.Forms.ComboBox conParCb;
        private System.Windows.Forms.DateTimePicker conFinParDtp;
        private System.Windows.Forms.RadioButton conFinChequeRb;
        private System.Windows.Forms.RadioButton conFinCartRb;
        private System.Windows.Forms.RadioButton conFinDinRb;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label conFinConsultaLb;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label conFinPacienteLb;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label conFinAddOpLb;
        private System.Windows.Forms.Panel conOperPn;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel conTaxPn;
        private System.Windows.Forms.TextBox conTx12Tb;
        private System.Windows.Forms.TextBox conTx9Tb;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox conTx5Tb;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox conTx10Tb;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox conTx11Tb;
        private System.Windows.Forms.TextBox conTx7Tb;
        private System.Windows.Forms.TextBox conTx8Tb;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox conTx3Tb;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox conTx4Tb;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox conTx6Tb;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox conTx2Tb;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox conTx1Tb;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox conTx0Tb;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button conTaxSaveBt;
        private System.Windows.Forms.CheckBox conTaxEsteCh;
        private System.Windows.Forms.Label conTaxNameLb;
        private System.Windows.Forms.ComboBox conTaxNameCb;
        private System.Windows.Forms.Label conTaxName2Tb;
        private System.Windows.Forms.RadioButton conFinDebRb;
        private System.Windows.Forms.NumericUpDown conFinVezesNup;
        private System.Windows.Forms.RadioButton conFinParRb;
        private System.Windows.Forms.RadioButton conFinCredVistaRb;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label conSalvaLb;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label conLimparLb;
        private System.Windows.Forms.Button conPlanoAtualizaBt;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel conDetailsPn;
        private System.Windows.Forms.Label conDetXLb;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.NumericUpDown conDetParNup;
        private System.Windows.Forms.ListBox conDetParLbox;
        private System.Windows.Forms.TextBox conDetParTb;
        private System.Windows.Forms.Label conDetAddLb;
        private System.Windows.Forms.Label conDetLpLb;
        private System.Windows.Forms.TextBox conDetDetTb;
        private System.Windows.Forms.Label conDetIdLb;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button conNovaBt;
        private System.Windows.Forms.Label conDetNomeLb;
        private System.Windows.Forms.TextBox conDetParTotTb;
        private System.Windows.Forms.CheckBox conDetParCh;
        private System.Windows.Forms.Panel cad1Pn;
        private System.Windows.Forms.DateTimePicker cadNascDtp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel conDetParPn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox conDetValorTb;
        private System.Windows.Forms.CheckBox ConDetPgCh;
        private System.Windows.Forms.ComboBox conDetFormaCb;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label cad1AntLb;
        private System.Windows.Forms.Label cad1ProxLv;
        private System.Windows.Forms.Button cad1ConsultasBt;
        private System.Windows.Forms.Label cad1LimpaLb;
        private System.Windows.Forms.Label cad1XLb;
        private System.Windows.Forms.Panel consultasPn;
        private System.Windows.Forms.Panel finPn;
        private System.Windows.Forms.Label finLpLb;
        private System.Windows.Forms.Label finXLb;
        private System.Windows.Forms.Panel planoPn;
        private System.Windows.Forms.Label planoXLb;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Panel avalPn;
        private System.Windows.Forms.Label avalXLb;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.ToolStripMenuItem planoDeAtendimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avaliaçãoToolStripMenuItem;
        private System.Windows.Forms.Label planoNomeLb;
        private System.Windows.Forms.Label avalNomeLb;
        private System.Windows.Forms.ToolStripMenuItem fichaEvolutivaToolStripMenuItem1;
    }
}

